package com.airline.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.airline.models.Airplane;
import com.airline.models.Flight;
import com.airline.models.Passenger;
import com.airline.models.Pilot;

/**
 * Session Bean implementation class FlightService
 */
@Stateless
@LocalBean
public class FlightService {

    /**
     * Default constructor. 
     */
    public FlightService() {
        // TODO Auto-generated constructor stub
    }

    @PersistenceContext(unitName = "airline")
    EntityManager em;
    
    public void addFlight(Flight f, Airplane a) {
    	em.persist(f);
    	//em.persist(a); -- propagated and cascaded from flight and saved avtomatically
    }
    //dodamo pilota na posamezen let
    public void addPilotToFlight(String pilotId, String flightId) {
    	
    	
    	TypedQuery<Flight> fQuery = em.createNamedQuery("Flight.FindById", Flight.class);
    	fQuery.setParameter("id", Integer.parseInt(flightId));
    	Flight f = (Flight)fQuery.getSingleResult();
    	
    	
    	TypedQuery<Pilot> pQuery = em.createNamedQuery("Pilot.findById", Pilot.class);
    	pQuery.setParameter("id", Integer.parseInt(pilotId));
    	Pilot p = pQuery.getSingleResult();
    	
    	//dodamo novega pilota v ze obstoject list - bazo
    	List<Pilot> pList = f.getPilots();
    	pList.add(p);
    	f.setPilots(pList);
    	
    	p.setFlightForPilot(f);
    	
    }
    //mozen nacin dostopa do baze
    public void addPassengerToFlight(String passengerId, String flightId) {
    	//get the passenger by id
    	CriteriaBuilder builder = em.getCriteriaBuilder();
    	CriteriaQuery<Passenger> cPassenger = builder.createQuery(Passenger.class);
    	Root<Passenger> pRoot = cPassenger.from(Passenger.class);
    	cPassenger.select(pRoot).where(builder.equal(pRoot.get("id").as(Integer.class), passengerId));
    	TypedQuery<Passenger> pQuery = em.createQuery(cPassenger);
    	Passenger p = pQuery.getSingleResult();
    	
    	//get flight by id
    	builder = em.getCriteriaBuilder();
    	CriteriaQuery<Flight> cFlight = builder.createQuery(Flight.class);
    	Root<Flight> fRoot = cFlight.from(Flight.class);
    	cFlight.select(fRoot).where(builder.equal(fRoot.get("id").as(Integer.class), flightId));
    	TypedQuery<Flight> fQuery = em.createQuery(cFlight);
    	Flight f = fQuery.getSingleResult();
    	
    	//Assosiate the passenger with thr flight
    	List<Passenger> pList = f.getPassengers();
    	pList.add(p);
    	f.setPassengers(pList);
    	
    	p.getFlights().add(f);
    	    	
    }
    
    //get all flights
    public List<Flight> getFlights() {
    	TypedQuery<Flight> query = em.createQuery("SELECT f FROM Flight f", Flight.class);
    	return query.getResultList();
    }
    
}
