package com.airline.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.airline.models.Flight;
import com.airline.models.Pilot;

/**
 * Session Bean implementation class PilotService
 */
@Stateless
@LocalBean
public class PilotService {
	
	@PersistenceContext(name = "airline")
	public EntityManager em;

    /**
     * Default constructor. 
     */
    public PilotService() {
        // TODO Auto-generated constructor stub
    }
    
    //dodamo pilota na posamezen let
    public void addNewPilotToFlight(Pilot p, String flightId) {
    	
    	//novega pilota najprej shranimo v bazo - nato ga dodamo letu
    	em.persist(p);
    	
    	TypedQuery<Flight> fQuery = em.createNamedQuery("Flight.FindById", Flight.class);
    	fQuery.setParameter("id", Integer.parseInt(flightId));
    	Flight f = (Flight)fQuery.getSingleResult();
    	
    	
    	//dodamo novega pilota v ze obstoject list - bazo
    	List<Pilot> pList = f.getPilots();
    	pList.add(p);
    	f.setPilots(pList);
    	
    	p.setFlightForPilot(f);
    	
    }
    
    public void addPilot(Pilot p) {
    	em.persist(p);
    }

}
