package com.airline.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.airline.models.Flight;
import com.airline.models.Passenger;
import com.airline.models.Pilot;

/**
 * Session Bean implementation class PassengerService
 */
@Stateless
@LocalBean
public class PassengerService {

    /**
     * Default constructor. 
     */
    public PassengerService() {
        // TODO Auto-generated constructor stub
    }
    
    @PersistenceContext(unitName = "airline")
    private EntityManager em;
    
    public void addPassenger(Passenger p) {
    	em.persist(p);
    }
    
    public void addFlightTicketToPassenger(String flightId, String passengerId) {
    	//get the passenger by id
    	CriteriaBuilder builder = em.getCriteriaBuilder();
    	CriteriaQuery<Passenger> cPassenger = builder.createQuery(Passenger.class);
    	Root<Passenger> pRoot = cPassenger.from(Passenger.class);
    	cPassenger.select(pRoot).where(builder.equal(pRoot.get("id").as(Integer.class), passengerId));
    	TypedQuery<Passenger> pQuery = em.createQuery(cPassenger);
    	Passenger p = pQuery.getSingleResult();
    	
    	//get flight by id
    	builder = em.getCriteriaBuilder();
    	CriteriaQuery<Flight> cFlight = builder.createQuery(Flight.class);
    	Root<Flight> fRoot = cFlight.from(Flight.class);
    	cFlight.select(fRoot).where(builder.equal(fRoot.get("id").as(Integer.class), flightId));
    	TypedQuery<Flight> fQuery = em.createQuery(cFlight);
    	Flight f = fQuery.getSingleResult();
    	
    	//Assosiate the flight with thr passenger
    	List<Flight> fList = p.getFlights();
    	fList.add(f);
    	p.setFlights(fList);
    	
    	f.getPassengers().add(p);
    }
    
    //get list of all passengers
    public List<Passenger> getPassengers(){
    	TypedQuery<Passenger> query = em.createQuery("SELECT p FROM Passenger p", Passenger.class);
    	return query.getResultList();
    }

}
