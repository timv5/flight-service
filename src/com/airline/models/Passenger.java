package com.airline.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Passenger
 *
 */
@Entity
public class Passenger implements Serializable {

	// nocemo da se shrani v bazo - da predstavlja atribut v bazi
	@Transient
	private static final long serialVersionUID = 1L;

	public Passenger() {
		super();
	}

	// sam generira id-je za vsak vnos v bazo
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer id;

	private String firstName;

	private String lastName;

	@Temporal(TemporalType.DATE)
	private Date dob;

	// brez te anotacije bi delalo ampak brez te anotacije bo v bazo shranjen kot
	// digit (1,2,3,..) - stevilka zaporedje v Enum clasu - torej Male = 1
	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Enumerated(EnumType.STRING)
	private FlightClass flightClass;
	
	//povezan z private List passengers ki se nahaja v Flight.java
	@ManyToMany(mappedBy = "passengers")
	private List<Flight> flights;	//the flights tickets the passenger has

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public FlightClass getFlightClass() {
		return flightClass;
	}

	public void setFlightClass(FlightClass flightClass) {
		this.flightClass = flightClass;
	}

	public List<Flight> getFlights() {
		return flights;
	}

	public void setFlights(List<Flight> flights) {
		this.flights = flights;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob
				+ ", gender=" + gender + ", flightClass=" + flightClass + ", flights=" + flights + "]";
	}

}
